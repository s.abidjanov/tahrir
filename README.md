# tahrir uchun qisqartmalar



**`-`**   belgilangan so\`z yoki ibora o\`chiriladi

**`+`**  taqdim qilingan so\`z yoki ibora qo\`shiladi

**`~`**  belgilangan so\`z yoki ibora o\`chirilib, taqdim qilingan so\`z yoki ibora qo\`shiladi

**`:`**  y'ani 

**`?`**  savol, maslahat, taklif, tavsiya, ko\`rib chiqish kerak, shunday bo\`lsa yaxshi emasmi, e'tiborga olish kerak va hokazo

**`qy`** qo`shib yoziladi

**`ay`** ajratib yoziladi

**`/`** birdan ortiq javob borligini ifoda qilib, ‘yoki’ ma’nosida ishlatiladi
